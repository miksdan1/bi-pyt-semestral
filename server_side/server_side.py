from chat import Client, MessagesColors, print_message, BUFFER_SIZE
from argparse import ArgumentParser
import socket
import threading
import time

# ----------------------------------
# Python client/server chat by Daniel Miks
# ----------------------------------
# Launch typing:
# py server_side.py <IP> <PORT>
# for example:
# py server_side.py 192.168.0.140 3333
# ----------------------------------

# load arg parse
cl_parse = ArgumentParser()

# parse IP and PORT
cl_parse.add_argument('ip', help='type ip of this machine')
cl_parse.add_argument('port',type=int,
                      help='type port on which you want to lauunch server')

# load arguments
cl_args = cl_parse.parse_args()

# declare TCP constansts
IP = cl_args.ip
PORT = int(cl_args.port)

# declare server name
SERVER_NAME = "SERVER"

print_message(SERVER_NAME, "Chat launched on {} {}".format(IP, PORT),
              MessagesColors.SERVER_MESSAGE)

# start server
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((IP, PORT))
server.listen()


# listen for clients
def server_listen():
    while True:
        try:
            # wait for new client
            client_socket, client_ip = server.accept()
            client = Client(client_socket, client_ip)

            # inform server about new connnection
            print_message(SERVER_NAME,
                          "New connection from: {}".format(client.ip[0]),
                          MessagesColors.CLIENT_JOIN)

            # ask client for nick
            client.socket.send('UI#'.encode('utf-8'))
            client.nick = client.socket.recv(BUFFER_SIZE).decode('utf-8')
            print_message(client.nick, "Joined to chat",
                          MessagesColors.CLIENT_JOIN)
            Client.broadcast(client,
                             ("JN#" + client.nick + "#").encode('utf-8'))

            # add client to list
            Client.client_list.append(client)

            # handle client to private thread
            client.handle()
        except OSError:
            # Except by stopping socket
            print_message(SERVER_NAME, "Server closed...Exiting...",
                          MessagesColors.SERVER_MESSAGE)
            Client.disconnect_all(Client)
            break
        except:
            print_message(SERVER_NAME, "Server error...Exiting...",
                          MessagesColors.SERVER_MESSAGE)
            Client.disconnect_all(Client)
            break

listening_thread = threading.Thread(target=server_listen)
listening_thread.start()

while True:
    try:
        input_command = ""
        # wait for server stop by command
        while input_command not in ["/stop"]:
            input_command = input("")
            # if input is not in commands print as broadcast message
            if input_command not in ["/stop"]:
                print_message(SERVER_NAME, input_command,
                              MessagesColors.SERVER_MESSAGE)
                Client.broadcast(Client, ("BC#" + SERVER_NAME + "#" +
                                          input_command).encode('utf-8'))
        server.close()
        print_message(SERVER_NAME, "Stopping by command",
                      MessagesColors.SERVER_MESSAGE)
        break
    # except Ctrl-C interrupt
    except KeyboardInterrupt:
        server.close()
        print_message(SERVER_NAME, "Stopping by keyboard interrupt",
                      MessagesColors.SERVER_MESSAGE)
        break
