import socket
import threading
from chat_methods import MessagesColors, print_message, BUFFER_SIZE

class Client:
    # variables of client
    socket = None
    ip = None
    nick = None
    client_list = []
    is_connected = True

    # constructor  - create client instance on client and ip
    def __init__(self, socket, ip):
        self.socket = socket
        self.ip = ip

    def handle(self):
        thread = threading.Thread(target=self.handle_loop)
        thread.start()

    # cast message to single client
    def unicast(self, message):
        self.socket.send(message)

    # brodcast byte message to each client
    @staticmethod
    def broadcast(self, message, include_self=True):
        for client in self.client_list:
            if client.socket != self.socket or include_self:
                client.socket.send(message)

    # end connectin to every single client
    @staticmethod
    def disconnect_all(self):
        for client in self.client_list:
            client.is_connected = False
            client.socket.close()

    # listen for messages from client
    def handle_loop(self):
        while self.is_connected:
            try:
                # waiting for receiving bytes from client
                message = self.socket.recv(BUFFER_SIZE)
                self.handle_message(message)
            except:
                # in case eror - kick off client
                print_message(self.nick, "Left chat",
                              MessagesColors.CLIENT_LEAVE)
                # brodcast leave message of client
                if self.is_connected:
                    self.broadcast(self,
                                   ("LV#" + self.nick + "#").encode('utf-8'),
                                   False)
                self.socket.close()
                self.client_list.remove(self)
                break

    def handle_message(self, message):
        try:
            message_decoded = message.decode('utf-8')
            message_without_prefix = message_decoded[3:]
            # IN CASE BROADCAST
            if message_decoded.startswith("BC"):
                print_message(self.nick, message_without_prefix,
                              MessagesColors.BRODCAST)
                self.broadcast(self, ("BC#" + self.nick + "#" +
                                      message_without_prefix).encode('utf-8'))
                return
            # IN CASE DIRECT MESAGE
            elif message_decoded.startswith("DM"):
                # split message by "#" key
                direct_arr = message_without_prefix.split('#')
                # linear search user
                for client in self.client_list:
                    # compare finding nickname
                    if client.nick == direct_arr[0]:
                        print_message(self.nick + " -> " + direct_arr[0],
                                      direct_arr[1], MessagesColors.DIRECT)
                        # send messeage to receiver
                        client.unicast(("DM#1#" + self.nick + "#" +
                                        direct_arr[1]).encode('utf-8'))
                        # send messeage to sender
                        self.unicast(("DM#0#" + client.nick + "#" +
                                      direct_arr[1]).encode('utf-8'))
                        return
                # print error in case user not found in list
                print_message(self.nick + " -> " + direct_arr[0],
                              "User not found...message will not be send",
                              MessagesColors.ERROR)
                self.unicast(("DE#0#" + direct_arr[0] + "#" +
                              "User not found...message will not be send")
                             .encode('utf-8'))
        except:
            pass
