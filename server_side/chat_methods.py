import os
import enum

os.system("")


class MessagesColors(enum.Enum):
    SERVER_MESSAGE = "\033[33m"  # yellow
    CLIENT_JOIN = "\033[92m"  # lightgreen
    CLIENT_LEAVE = "\033[91m"  # red
    BRODCAST = "\033[94m"  # lightblue
    DIRECT = "\033[35m"  # lightcyan
    ERROR = "\033[91m"  # red
    STANDARD = "\033[0m"

BUFFER_SIZE = 1024


def print_message(author, message, type=MessagesColors.STANDARD):
    print(type.value + "[" + author + "]: " + message + "\033[0m")
