import socket
import threading
from chat_methods import MessagesColors, BUFFER_SIZE, print_message

CHAT_NAME = "CHAT"


class MessageHandler:
    socket = None
    nick = None
    active = True

    def receive_loop(self):
        while self.active:
            try:
                # Receiving bytes from chat server
                message = self.socket.recv(BUFFER_SIZE).decode('utf-8')
                #
                self.message_handle(message)
            except:
                # Close socket when receiving fail
                print_message(CHAT_NAME,
                              "Connection error... Dsiconnecting...",
                              MessagesColors.SERVER_MESSAGE,
                              self.console, self.ui)
                self.stop_threads()
                break

    def send_loop(self):
        while self.active:
            try:
                input_from_client = input('')
                output = self.output_handle(input_from_client)
                self.socket.send(output)
            except:
                # Except by stopping socket
                print_message(CHAT_NAME, "Exiting...",
                              MessagesColors.SERVER_MESSAGE,
                              self.console, self.ui)
                self.stop_threads()
                break

    def message_handle(self, message):
        message_without_prefix = message[3:]
        message_parsed = message_without_prefix.split('#')
        if message == 'UI#':  # UI - user idenification
            self.socket.send(self.nick.encode('utf-8'))
        elif message.startswith("BC"):
            print_message(message_parsed[0],
                          message_parsed[1], MessagesColors.BRODCAST,
                          self.console, self.ui)
        elif message.startswith("JN"):
            print_message(message_parsed[0], "Joined to chat",
                          MessagesColors.CLIENT_JOIN, self.console, self.ui)
        elif message.startswith("LV"):
            print_message(message_parsed[0], "Left chat",
                          MessagesColors.CLIENT_LEAVE, self.console, self.ui)
        elif message.startswith("DM"):
            if message_parsed[0] == "0":
                print_message(self.nick + " -> " + message_parsed[1],
                              message_parsed[2], MessagesColors.DIRECT,
                              self.console, self.ui)
            else:
                print_message(message_parsed[1] + " -> " +
                              self.nick, message_parsed[2],
                              MessagesColors.DIRECT, self.console, self.ui)
        elif message.startswith("DE"):
                print_message(self.nick + " -> " +
                              message_parsed[1], message_parsed[2],
                              MessagesColors.ERROR, self.console, self.ui)

    def output_handle(self, input):
        if input == "":
            return None
        if input.startswith("/direct"):
            input = input[7:].strip()
            index = input.find(" ")
            name = input[:index].strip()
            message = input[index:].strip()
            return ("DM#" + name + "#" + message).encode('utf-8')
        elif input.startswith("/exit"):
            self.stop_threads()
            return "".encode('utf-8')

        else:
            return ("BC#" + input).encode('utf-8')

    def stop_threads(self):
        self.active = False
        self.socket.close()

    def __init__(self, socket, nick, console=True, ui=None):
        self.ui = ui
        self.console = console
        self.socket = socket
        self.nick = nick
        if not socket:
            return
        self.receive_thread = threading.Thread(target=self.receive_loop)
        self.receive_thread.start()
        if console:
            self.write_thread = threading.Thread(target=self.send_loop)
            self.write_thread.start()
