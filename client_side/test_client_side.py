import pytest
import socket
from chat import *


def test_output_handle_1():
    mh = MessageHandler(None, None)
    message = mh.output_handle("/direct dog hello")
    assert(message == "DM#dog#hello".encode('utf-8'))


def test_output_handle_2():
    mh = MessageHandler(None, None)
    message = mh.output_handle("hello there!")
    assert(message == "BC#hello there!".encode('utf-8'))


def test_output_handle_3():
    mh = MessageHandler(None, None)
    message = mh.output_handle("")
    assert(message is None)
