from chat import MessageHandler,MessagesColors, BUFFER_SIZE, print_message, CHAT_NAME
from PyQt5 import QtWidgets, uic
import sys
import socket


class Ui(QtWidgets.QMainWindow):
    mh = None

    def __init__(self):
        super(Ui, self).__init__()
        uic.loadUi('gui.ui', self)
        self.init_gui()
        self.show()

    def init_gui(self):
        self.send_btn.clicked.connect(self.send)
        self.connect_btn.clicked.connect(self.connect_chat)

    def connect_chat(self):
        IP = self.ip_box.toPlainText()
        PORT = int(self.port_box.toPlainText())
        NICK = self.nick_box.toPlainText()

        # start cllient
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((IP, PORT))

        # inform user - can start chatting
        print_message(CHAT_NAME, "Now, you can use chat",
                      MessagesColors.SERVER_MESSAGE)

        # create message handler for receiving and sending messages
        self.mh = MessageHandler(client, NICK, False, ui=self)

    def send(self):
        try:
            input_from_client = self.msg_box.toPlainText()
            output = self.mh.output_handle(input_from_client)
            self.mh.socket.send(output)
        except:
            # Except by stopping socket
            print_message(CHAT_NAME, "Exiting...",
                          MessagesColors.SERVER_MESSAGE)
            self.mh.stop_threads()


app = QtWidgets.QApplication(sys.argv)
window = Ui()
app.exec_()
