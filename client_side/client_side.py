from chat import MessageHandler,MessagesColors, BUFFER_SIZE, print_message, CHAT_NAME
from argparse import ArgumentParser
import socket
import threading


# ----------------------------------
# Python client/server chat by Daniel Miks
# ----------------------------------
# Launch typing:
# py client_side.py <IP> <PORT>
# for example:
# py client_side.py 192.168.0.140 3333
# ----------------------------------

# load arg parse
cl_parse = ArgumentParser()

# parse IP and PORT
cl_parse.add_argument('ip', help='type ip of chat server you want to connect')
cl_parse.add_argument('port',type=int, help='type port of server')

# load arguments
cl_args = cl_parse.parse_args()

# declare TCP constansts
IP = cl_args.ip
PORT = int(cl_args.port)


# ask user for nickname
nick = ""
while nick == "" or " " in nick:
    print_message(CHAT_NAME, "Choose your nickname (without space):",
                  MessagesColors.SERVER_MESSAGE)
    nick = input("")

# start cllient
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((IP, PORT))

# inform user - can start chatting
print_message(CHAT_NAME, "Now, you can use chat",
              MessagesColors.SERVER_MESSAGE)

# create message handler for receiving and sending messages
mh = MessageHandler(client, nick)
