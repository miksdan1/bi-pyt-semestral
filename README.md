# Chatovací aplikace Client/Server
**Author:** Daniel Mikš \
**Email to Author:** miksdan1@fit.cvut.cz 

## Spuštění serveru

```bash
py server_side.py <IP> <PORT>
```
## Spuštění klienta

```bash
py client_side.py <IP> <PORT>
```
### Spuštění GUI
- GUI je možné spustit pomocí dvojkliku (prostřednictvím aplikace python)
- případně konzolově
```bash
py gui.py
```
- následně je nutné se připojit na chatovací server s nickem
- pak je možné libovolně chatovat pomocí příkazů na pravé staraně GUI
### Spuštění testů
- nutné mít nainstalovaný pytest
```bash
pip install pytest
```
- samotné spuštění testu
```bash
py -m pytest
```
- nebo pouze pomocí
```bash
pytest
```
## Příkazy pro ovládání
### Client
-  /direct [nick] [message] - type for direct message to user
-  /exit - type for exit from chat
- [message] - type for broadcast message
### Server
- /stop - for stopping server
- [message] - type for server broadcast message

## Použité barvy pro výpis
| ENUM KEY       | KÓD BARVY   | BARVA      |
| -------------- | ----------- | ---------- |
| SERVER_MESSAGE | "\033[33m"  | yellow     |
| CLIENT_JOIN    | "\033[92m"  | lightgreen |
| CLIENT_LEAVE   | "\033[91m"  | red        |
| BRODCAST       | "\033[94m"  | lightblue  |
| DIRECT         | "\033[35m"  | lightcyan  |
| ERROR          | "\033[91m"  | red        |
| STANDARD       | "\033[0m"   |            |

## Prefixy pro přenos zpráv
### Klient -> Server
 - DM (Direct message) "DM#[receiver]#[message]"
 - BC (Broadcast) "BC#[message]"

### Server -> Klient
 - UI (User identification - request) "UI#"
 - DM (Direct message) "DM#[role - 0/1]#[sender]#[message]"
 - DE (Direct error - when receiver not exist) "DE#[role - 0/1]#[receiver]#[error]"
 - BC (Broadcast) "BC#[sender]#[message]"
 - JN (User Join chat) "JN#[nick]"
 - LV (User Leave chat) "LV#[nick]"

## Screenshots
Vzorová ukazka náhledu funkčnosti aplikace \
**Náhled klienta:** \
![client_side](https://gitlab.fit.cvut.cz/miksdan1/bi-pyt-semestral/raw/198d265a2981c0412f14549268e71d1d4330f7ea/screenshots/client_side.PNG) 

**Náhled serveru:** \
![server_side](https://gitlab.fit.cvut.cz/miksdan1/bi-pyt-semestral/raw/198d265a2981c0412f14549268e71d1d4330f7ea/screenshots/server_side.PNG)

**Náhled klientského GUI:** \
![client_gui](https://gitlab.fit.cvut.cz/miksdan1/bi-pyt-semestral/raw/176a99bbe880409190dfb4857190c88aa3e34139/screenshots/client_gui.PNG)

## Git repozitář
Pro verzování bylo užito veřejného repozitáře na univerzitním [GitLabu](https://gitlab.fit.cvut.cz/miksdan1/bi-pyt-semestral)
